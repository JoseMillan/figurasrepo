
package jerarquiadeclases;


public class Professor extends Personal{
    private String titulacio;
    private String especialitat;

    public Professor(String nombre, String direc, String numeSS, String titulo,String espe) {
        super(nombre, direc,numeSS);
        especialitat=espe;
        titulacio=titulo;
    }
    
    public void setTitulacio(String titulo){
        titulacio = titulo;
    }
    
    public String getTitulacio(){
        return titulacio;
    }
     public void setEspecialitat(String esp){
        especialitat = esp;
    }
    
    public String getEspecialitat(){
        return especialitat;
    }
}
