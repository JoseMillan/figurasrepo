/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prachilos;

import java.util.Date;
import java.util.Stack;

/**
 *
 * @author Jose
 */
public class Hilo extends Thread {
    
    long tiempoInicial = new Date().getTime();;
    Stack<Integer> numbers;
    
    public Hilo(Stack<Integer> numbers){
        this.numbers = numbers;
    }
    
    @Override
    public void run(){

        while (!numbers.isEmpty()){
            Integer number = numbers.pop();
            
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Se ha procesado el numero " + number);
            
                            }
        
          System.out.println("Ha tardado " + (new Date().getTime() - tiempoInicial)/1000 + " segundos"); 
        
       

    }
    
    
}
